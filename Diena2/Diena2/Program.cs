﻿using System;

namespace Diena2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ievadiet datumu. Vispirms ievadiet dienu:");
            string diena = Console.ReadLine();
            int dienaNumurs = Convert.ToInt32(diena);

            Console.WriteLine("Tagad ievadiet menesi:");
            string menesis = Console.ReadLine();
            int menesisNumurs = Convert.ToInt32(menesis);

            Console.WriteLine("Un ievadiet gadu:");
            string gads = Console.ReadLine();
            int gadsNumurs = Convert.ToInt32(gads);

            //1.lietotājs ievada datumu
            // int diena = 1, menesis = 2, gads = 2000;

            //2.Izveido datuma objektu
            DateTime datums = new DateTime(gadsNumurs, menesisNumurs, dienaNumurs);

            //3. Nolasīt DayOfWeek no datuma

            int dayOfWeek = (int)datums.DayOfWeek;
                            
            switch (dayOfWeek)
            {
                case (int)DayOfWeek.Sunday:
                    Console.WriteLine("Šī ir svētdiena");
                    break;
                case (int)DayOfWeek.Monday:
                    Console.WriteLine("Šī ir pirmdiena");
                    break;
                case (int)DayOfWeek.Tuesday:
                    Console.WriteLine("Šī ir otrdiena");
                    break;
                case (int)DayOfWeek.Wednesday:
                    Console.WriteLine("Šī ir trešdiena");
                    break;
                case (int)DayOfWeek.Thursday:
                    Console.WriteLine("Šī ir ceturdiena");
                    break;
                case (int)DayOfWeek.Friday:
                    Console.WriteLine("Šī ir piektdiena");
                    break;
                case (int)DayOfWeek.Saturday:
                    Console.WriteLine("Šī ir sestdiena");
                    break;
                default:
                    Console.WriteLine("Diena netika atrasta!");
                    break;
            }

            if(dayOfWeek == (int)DayOfWeek.Saturday || dayOfWeek == (int)DayOfWeek.Sunday)
            {
                Console.WriteLine("brivdiena");
            }
            else
            {
                Console.WriteLine("darbadiena");
            }
            Console.ReadKey();
        }
    }
}
