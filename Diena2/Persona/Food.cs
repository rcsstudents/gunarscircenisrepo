﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Persona
{
    public abstract class Food
    {
        //Nesatur funkcionilitāti, obligāti vajag override šo metodi
       // public abstract void DrinkAbstract();

        // Satur funkcionilitāti, var override šo metodi, ja vajag
        public virtual void Drink()
        {
            Console.WriteLine("I drink beer");
        }

        // Satur funkcionitāti, nevar override
        public void Eat()
        {
            Console.WriteLine("I like food");
        }
    }
}
