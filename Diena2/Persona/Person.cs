﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Persona
{
    class Person : Food, IWalking
    {
        public string Name;
        public double Height;
        public double Weight;
        public string HairColor;
        public DayOfWeek FavoriteWeekDay;
        public Person(string name, double height, double weight, string hairColor, DayOfWeek favoriteWeekDay)
        {
            Name = name;
            Height = height;
            Weight = weight;
            HairColor = hairColor;
            FavoriteWeekDay = favoriteWeekDay;
        }
        public virtual void WritePersonInfo()
        {
            Console.WriteLine($"Persona {Name} ir {Height} cm gara un sver {Weight} kg");
            Console.WriteLine($"Matu krāsa ir {HairColor}");
            Console.WriteLine($"milāka'nedelas diena ir {FavoriteWeekDay}");
        }
                
        public void DoWalking()
        {
            Console.WriteLine("Persona nogāja 10km");
        }

        public override void Drink()
        {
            Console.WriteLine("Person drinks beer");
        }

    }
}
