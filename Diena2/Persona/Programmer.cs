﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Persona
{
    class Programmer : Person
    {
        public string ProgrammingLanguage;

        public Programmer(string name, double height, double weight, string hairColor, string programmingLanguage):
        base(name, height, weight, hairColor, DayOfWeek.Monday)
            {
            ProgrammingLanguage = programmingLanguage;
            }
        public override void WritePersonInfo()
        {
            Console.WriteLine($"Progammetajs {Name} ir {Height} cm gars un sver {Weight} kg");
            Console.WriteLine($"Matu krāsa ir {HairColor}");
            Console.WriteLine($"milāka'nedelas diena ir {FavoriteWeekDay}");
        }
    }
}
