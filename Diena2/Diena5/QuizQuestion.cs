﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diena5
{
    [Serializable]
    public class QuizQuestion
    {
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}
