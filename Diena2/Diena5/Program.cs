﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace Diena5
{
    class Program
    {
        static void Main(string[] args)
        {
          // using (StreamWriter writer = new StreamWriter("teksts.txt"))
           // {
                //writer.WriteLine("Pirmais teksts failā, madafakaaaaa");    //  ieraksta tekstu, bet katreiz atverot nodzēš iepriekšējo
                
         //   }
            using (StreamWriter writer = File.AppendText("teksts.txt"))
            {
                writer.WriteLine("Otrais teksts failā");
            }
            // File.WriteAllText("teksts.txt", String.Empty);   nodzēš visu kas ierakstīts failā
            List<string> tekstsSaraksts = new List<string>();
            using (StreamReader reader = new StreamReader("teksts.txt"))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    tekstsSaraksts.Add(line);
                }

            }

            foreach(string tekstsRinda in tekstsSaraksts)
            {
                Console.WriteLine(tekstsRinda);
            }

            QuizQuestion quizQuestion = new QuizQuestion();
            quizQuestion.Question = "Jautājums?";
            quizQuestion.Answer = "Atbilde?";

            string json = JsonConvert.SerializeObject(quizQuestion);


            QuizQuestion dederialized = JsonConvert.DeserializeObject<QuizQuestion>(json);
            Console.ReadKey();
        }
    }
}
