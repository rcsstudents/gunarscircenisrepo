﻿using System;
using System.Collections.Generic;

namespace Uzdevums1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IPerson> persons = new List<IPerson>();


            persons.Add(new Professor("Artis", "Kautkas"));
            persons.Add(new Student("Jānis", "Koks"));

           
            foreach (IPerson person in persons)
            {
               string teksts = person.GetHomework();
                Console.WriteLine(teksts);
            }

            Console.ReadKey();
        }
    }
}
