﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uzdevums1
{
    public abstract class Person
    {
        public string Name;
        public string Surname;
        public string GetFullName()
        {
            return Name + " " + Surname;
        }
        public virtual void WritePersonInfo()
        {
            Console.WriteLine($"Persona {Name} {Surname}");
        }
            
    }
}
