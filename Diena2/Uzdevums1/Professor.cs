﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uzdevums1
{
    public class Professor : Person, IPerson
    {
        
        public Professor(string name, string surname)            
        {
            Name = name;
            Surname = surname;
        }
        public string GetHomework()
        {
            string fullName = GetFullName();
            return $"Professor {fullName} is a pro";


        }

    }
}
