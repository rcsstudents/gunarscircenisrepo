﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uzdevums1
{
    public class Student : Person, IPerson
    { 
        
        public Student(string name, string surname)
        {
            Name = name;
            Surname = surname;
        }
        public string GetHomework()
        {
            string fullName = GetFullName();
            return $"Student {fullName} has no homework!";
        }

    }
}
