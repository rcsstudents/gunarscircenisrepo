﻿using DataManager;
using DataManager.ManageData;
using Models.EmployeeModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmployeeManagement
{
    public class EmployeeManager
    {
    public void ManageEmployees()
        {
            DataReader reader = new DataReader(DataConfiguration.EmployeeData);
            Employee employee = reader.ReadEmployee();

            Console.WriteLine($"Full name: {employee.FullName()}");
            Console.WriteLine($"Age: {employee.Age}");
            Console.WriteLine($"Vacation: {employee.Vacation.Reason}");
            Console.WriteLine($"VacationStar: {employee.Vacation.StartDate}");
            Console.WriteLine($"VacationEnd: {employee.Vacation.EndDate}");
        }
    }
}
