﻿using Models.EmployeeModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmployeeGenerator
{
    public class GenerateEmployee
    {
        private List<string> GenerateData(string dataType)
        {
            Console.WriteLine($"Write {dataType}");
            string data = Console.ReadLine();
            List<string> dataList = data.Split(' ').ToList();

            return dataList;
        }

        public Vacations GenerateVacation()
        {
            //expected format: "Reason StartEndDay month year EndDate month year
            //Piemēram: "Ceļojums 2018 10 5 2018 11 5 
            List<string> vacationData = GenerateData("Vacation (Reason, Star date, End date) ");
            Vacations vacation = new Vacations();
            vacation.Reason = vacationData[0];
            vacation.StartDate = new DateTime(
                Convert.ToInt32(vacationData[1]),
                Convert.ToInt32(vacationData[2]),
                Convert.ToInt32(vacationData[3]));

            vacation.EndDate = new DateTime(
                Convert.ToInt32(vacationData[4]),
                Convert.ToInt32(vacationData[5]),
                Convert.ToInt32(vacationData[6]));

            return vacation;
            
        }

        public Employee GenerateEmployee1()
        {
            
            Console.WriteLine("Vai ievadīsi vēl darbinieku? (y/n)");
            Console.ReadLine();
            while

            List<string> employeeData = GenerateData("Employee (Name, Surname, Age)");
            Employee employee = new Employee(); 
            employee.Name = employeeData[0]; 
            employee.Surname = employeeData[1]; 
            employee.Age = Convert.ToInt32(employeeData[2]); 
            employee.Vacation = GenerateVacation();

            return employee;

        }
    }
}
