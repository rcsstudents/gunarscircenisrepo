﻿using DataManager;
using DataManager.ManageData;
using Models.EmployeeModels;
using System;

namespace EmployeeGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to employee generator!");
            try
            {
                GenerateEmployee generateEmployee = new GenerateEmployee();
                Employee employee = generateEmployee.GenerateEmployee1();

                DataWriter writer = new DataWriter(DataConfiguration.EmployeeData);
                writer.WriteEmployee(employee);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
        }
    }
}
