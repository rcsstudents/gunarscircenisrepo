﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.EmployeeModels
{
    [Serializable]
    public class Vacations
    {
        public string Reason { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
