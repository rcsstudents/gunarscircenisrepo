﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diena1
{
    public static class DefinedQuestion
    {
        public static List<QuizQuestion> quizQuestion = new List<QuizQuestion>
        {
           new QuizQuestion
           {
            Question = "Latvijas galvaspilsēta ir?",
            Answer = "Riga"
           },
           new QuizQuestion
           {
            Question = "Krutākais Rīgas mikrorajons ir?",
            Answer = "Mazais Purvciems"
           },
           new QuizQuestion
           {
            Question = "Labākais nakstlokāls Eiropas Savienībā?",
            Answer = "Vecais Peteris"
           }
        };
    }
}
