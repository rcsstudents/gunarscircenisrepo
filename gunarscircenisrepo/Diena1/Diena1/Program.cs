﻿using System;
using System.Collections.Generic;

namespace Diena1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> userAnswers = new List<string>();
            int score = 0;
           // string answer = string.Empty; // ir tas pats kas: string answer = ""  
           // uzdodam jautājumus
           foreach(QuizQuestion question in DefinedQuestion.quizQuestion)
            {
                Console.WriteLine(question.Question);
                string answer = Console.ReadLine();
                if(answer == question.Answer)
                {
                    score++;
                }
                userAnswers.Add(answer);

            }

            // while(i<DefinedQuestion.quizQuestion.Count)         while ciklam vispirms arī i vajag nodefinēt
            // {
            //    Console.WriteLine(DefinedQuestion.quizQuestion[i].Question);
            //        answer += Console.ReadLine() + "\n";
            //    i++;
            // }
            //for (int i = 0; i < DefinedQuestion.quizQuestion.Count; i++)
            // {
            //     Console.WriteLine(DefinedQuestion.quizQuestion[i].Question);
            //    answer = answer + Console.ReadLine() + "\n";
            // }
            // izdrukā atbildes
            for (int i = 0; i < userAnswers.Count; i++)
            {
                Console.WriteLine("Pareizā atbilde: " + DefinedQuestion.quizQuestion[i].Answer);
                Console.WriteLine("Tu atbildēji: " + userAnswers[i]);
            }
            Console.WriteLine("Rezultāts ir: " + score);
            Console.WriteLine("Max punktu skaits: " + userAnswers.Count);

            Console.ReadKey();

        }
            

    }
}
