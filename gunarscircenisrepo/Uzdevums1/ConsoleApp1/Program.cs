﻿using DatuTipi;
using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<MyDataTypes> myDataTypesList = new List<MyDataTypes>();

            Console.WriteLine("Izpildīt programmu (y/n)");
            while (Console.ReadLine() != "n")
            {
                MyDataTypes myDataTypes = new MyDataTypes();
                //1. BOOL
                Console.WriteLine("Ievadiet bool vērtību (true/false)");
                string myBoolString = Console.ReadLine();
                myDataTypes.MyBool = Convert.ToBoolean(myBoolString);

                //2.BYTE
                Console.WriteLine("Ievadiet byte vērtību (0 - 255)");
                string myByte = Console.ReadLine();

                bool isConverted = byte.TryParse(myByte, out byte result);
                if (isConverted == true)
                {
                    myDataTypes.MyByte = Convert.ToByte(myByte);
                }

                //3.SHORT

                Console.WriteLine("Ievadiet short vērtību ");
                string myShort = Console.ReadLine();
                myDataTypes.MyShort = Convert.ToInt16(myShort);


                //INT
                Console.WriteLine("Ievadiet int vērtību ");
                string myInt = Console.ReadLine();
                myDataTypes.MyInt = Convert.ToInt32(myInt);

                //LONG
                Console.WriteLine("Ievadiet long vērtību ");
                string myLong = Console.ReadLine();
                myDataTypes.MyLong = Convert.ToInt64(myLong);

                //DOUBLE
                Console.WriteLine("Ievadiet double vērtību ");
                string myDouble = Console.ReadLine();
                myDataTypes.MyDouble = Convert.ToDouble(myDouble);

                //DECIMAL
                Console.WriteLine("Ievadiet decimal vērtību ");
                string myDecimal = Console.ReadLine();
                myDataTypes.MyDecimal = Convert.ToDecimal(myDecimal);

                //STRING
                Console.WriteLine("Ievadiet string vērtību ");
                string myString = Console.ReadLine();
                myDataTypes.MyString = Convert.ToString(myString);

                //CHAR
                Console.WriteLine("Ievadiet char vērtību ");
                string myChar = Console.ReadLine();
                myDataTypes.MyChar = Convert.ToChar(myChar);

                myDataTypesList.Add(myDataTypes);
                      
            }

            foreach(MyDataTypes myData in myDataTypesList)
            {
                Console.WriteLine(" MyBool = " + myData.MyBool);
            }
             Console.ReadKey();

        }
    }
}