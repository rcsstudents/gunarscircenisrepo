﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uzdevums1
{
    public static class MyDataTypes
    {
        //bool, byte, short, int, long, double, decimal, string, char
        public static bool MyBool { get; set; }
        public static byte MyByte { get; set; }
        public static short MyShort { get; set; }
        public static int MyInt { get; set; }
        public static long MyLong { get; set; }
        public static double MyDouble { get; set; }
        public static decimal MyDecimal { get; set; }
        public static string MyString { get; set; }
        public static char MyChar { get; set; }

    }
}
