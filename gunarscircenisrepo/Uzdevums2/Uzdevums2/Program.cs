﻿using System;
using System.Linq;

namespace Uzdevums2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Vai izpildīt programmu (y/n)");
            while (Console.ReadLine() != "n") 
            {

                string garums;
                Console.WriteLine("Ievadiet vārdu: ");
                garums = Console.ReadLine();
                // a,e,i,u,o
                var a = garums.Count(x => x == 'a');
                var e = garums.Count(x => x == 'e');
                var i = garums.Count(x => x == 'i');
                var u = garums.Count(x => x == 'u');
                var o = garums.Count(x => x == 'o');


                Console.WriteLine(garums.Length);
                Console.WriteLine("a burti vārdā: " + a);
                Console.WriteLine("e burti vārdā: " + e);
                Console.WriteLine("i burti vārdā: " + i);
                Console.WriteLine("u burti vārdā: " + u);
                Console.WriteLine("o burti vārdā: " + o);

            }
            
            Console.ReadLine();
        }
    }
}
