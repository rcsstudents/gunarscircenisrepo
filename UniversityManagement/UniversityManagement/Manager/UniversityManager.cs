﻿using DataManager;
using DataManager.ManageData;
using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace UniversityManagement.Manager
{
    public class UniversityManager : IManager
    {
        List<University> Universities;
        DataReader reader;
        DataWriter writer;

        public UniversityManager()
        {
            reader = new DataReader(DataConfiguration.UniversityData);
            writer = new DataWriter(DataConfiguration.UniversityData);
            Universities = reader.ReadData<University>();
        }

        public void Create()
        {
            Console.WriteLine("University  - Create");
        }

        public void Delete()
        {
            Console.WriteLine("University  - Delete");
        }

        public void Read()
        {
            Console.WriteLine("University  - Read");
        }

        public void Update()
        {
            Console.WriteLine("University  - UPdate");
        }
    }
}
