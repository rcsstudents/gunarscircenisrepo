﻿using DataManager;
using DataManager.ManageData;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UniversityManagement.Manager
{
   public  class StudentManager : IManager
    {
        List<Student> Students;
        DataReader reader;
        DataWriter writer;

        public StudentManager()
        {
            reader = new DataReader(DataConfiguration.StudentData);
            writer = new DataWriter(DataConfiguration.StudentData);
            Students = reader.ReadData<Student>();
        }

        public void Create()
        {
            Console.WriteLine("Student  - Create");
            List<string> GenerateData(string dataType)
            {
                Console.WriteLine($"Write {dataType}");
                string data = Console.ReadLine();
                List<string> dataList = data.Split(' ').ToList();

                return dataList;
            }
            
            List<string> studentData = GenerateData("Student (ID, Name, Surname)");
            Student student = new Student();
            student.Id = Convert.ToInt32(studentData[0]);            
            student.Name = studentData[1];
            student.Surname = studentData[2];

            return;      
        }

        public void Delete()
        {
            Console.WriteLine("Student  - Delete");
        }

        public void Read()
        {
            Console.WriteLine("Student  - Read");
        }

        public void Update()
        {
            Console.WriteLine("Student  - Update");
        }
    }
}
